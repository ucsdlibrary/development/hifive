# Hi Five!
An application supporting an Employee Recognition program workflow

[![pipeline status](https://gitlab.com/ucsdlibrary/development/hifive/badges/trunk/pipeline.svg)](https://gitlab.com/ucsdlibrary/development/hifive/-/commits/trunk)
[![coverage report](https://gitlab.com/ucsdlibrary/development/hifive/badges/trunk/coverage.svg)](https://gitlab.com/ucsdlibrary/development/hifive/-/commits/trunk)

## Local Development
### Email / Letter Opener
To view emails submitted in a development context, you can navigate to:
```
http://localhost:3000/letter_opener
```

### Environment Variables
This app is using ENV-driven configuration options through
[dotenv](https://github.com/bkeepers/dotenv) for non-production environments.

To override environment variables set by `.env`, create a `.env.local` file: <https://github.com/bkeepers/dotenv#what-other-env-files-can-i-use>.  We recommend adding the private variables into the `.env.local` file to avoid the risk of accidentally committing an updated file that contains the password.

For the docker-compose setup, there is also a `.env.docker` file. This is used
for environment variables that are required for the
`docker/docker-entrypoint.sh` script that `dotenv` will not have made available
yet. This allows the application to load the environment dotfile when needed
(for development or test) while keeping these variables separate.

### Docker
1. Install docker and docker-compose
1. To spin up your environment, run `make up`
    - Run `make menu` to see all options

> If you wish to persist your local development data across sessions, you can
temporarily change the `DATABASE_COMMAND` to something like `db:migrate`

#### Employee Data

> Note: To load the seed data, your local machine must have the `shuf` command
> Install on Mac OS by: brew install coreutils

To load employee data locally, after running the docker commands above to start
up the local environment, you can run the `db:seed` rake command as follows:

You can use the Makefile:

`make seed`

Or you can invoke directly (development example):

`docker-compose exec web rake db:seed`

Note that you will need to either be on campus or using the VPN, as this
accesses LDAP data from Active Directory. You will also need LDAP account info to load the seed data.  This info can be found in LastPass. You can put the following LDAP variables  `APPS_H5_LDAP_HOST, APPS_H5_LDAP_PORT, APPS_H5_LDAP_BASE, APPS_H5_LDAP_USERNAME, APPS_H5_LDAP_PW, APPS_H5_LDAP_GROUP` into `.env.local` file.

#### Testing
1. For full test suite, run `make test`
1. Run individual tests with `docker-compose exec web` prefix for any RSpec command.

Examples:
```
docker-compose exec web bin/rails spec
docker-compose exec web bundle exec rake spec
docker-compose exec web bundle exec rake spec/models/user.rb
docker-compose exec web bundle exec rake spec/models/user.rb:50
...
```

### Troubleshooting
If you get odd errors about tmp or cached file permissions, particularly if you
are testing both the development and production docker environments, you should
try removing the cached files in the `./tmp` folder.

`sudo rm -rf ./tmp/cache`

#### JavaScript
The [`webpack-bundle-analyzer` plugin][webpack-analyzer] is installed as a dev dependency.

Once you `make up` you can navigate to `http://localhost:8888` to view the UI

#### Debugging
With docker-compose running, in a new terminal/tab attach to the container:
`docker attach hifive_web_1`

### Production Docker
The main `Dockerfile` is setup for production. If you want to run this locally,
you can use the `./bin/docker-helper.sh -p` script.

Alternatively, you can directly deploy the High Five helm chart. See below.

Note you _cannot_ run the test suite in this environment. The tooling is not in place.

## Deploying with GitLab CI/CD & Helm
The application currently uses the [gitlab-ci-tools][gitlab-ci-tools] templates
for deploying and managing `review` and `staging` applications.

### Values Files
The helm chart deployments for CI expect base64-encoded values `yaml` files. If
you need to update or reference these files, you may copy them from GitLab and
`base64 -d` them. Or, you can access them in LastPass via the entry `HighFive -
helm values files`.

*Note*: Please make sure any changes are reflect in both the CI/CD variable(s) and
the LastPass entry!

### Review Apps
A successful Merge Request pipeline will result in a Helm deployment to our
Rancher cluster with an application dedicated to the code as it exists in the
Merge Request. This environment:

* Will have all current production data (as of the previous day) loaded into it
* Will use the `production` `Dockerfile` target, exactly as the `production` and
    `staging` environments
* Will use the same helm chart as the `production` and `staging` environments.

Differences in Review Apps:
* Authentication uses local `developer` auth. It is (currently) not possible to
    dynamically generate Google Auth credentials for a Review App wildcard-style
    DNS entry.
* Will not use TLS cert for SSL termination for ingress. This app will run over `http`.

#### Cleanup
Upon a successful merge (or close) of a Merge Request the Review App will automatically
run a cleanup job.

Alternatively, if the Merge Request lasts longer than a week, the Review App will be
automatically removed from Rancher/k8s.

### Staging
A successful `trunk` pipeline run will result in a Helm deployment to our
Rancher cluster, within the `highfive-staging` namespace.

This deployment is identical to Production and only differs in the environment
variable values for things like TLS cert, ingress URL, and database credentials.

### Production
A successful `trunk` pipeline run will result in a `manual` `production_deploy`
job creation. This allows selective `production` environment Helm deployments to
our Rancher cluster, within the `highfive-production` namespace.

This deployment is identical to Production and only differs in the environment
variable values for things like TLS cert, ingress URL, and database credentials.

### Rollback
A `manual` `rollback` job is created in case something goes wrong in the
deployment to `staging` or `production`. The provides a quick rollback option via the GitLab UI.
