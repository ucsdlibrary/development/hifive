# renovate: datasource=docker depName=ruby versioning=ruby
ARG RUBY_VERSION=2.7.2
FROM ruby:$RUBY_VERSION-alpine as development

RUN apk --no-cache upgrade && \
  apk add --no-cache \
  build-base \
  less \
  libxml2-dev \
  libxslt-dev \
  postgresql-dev \
  nodejs \
  nodejs-npm \
  tzdata \
  shared-mime-info \
  vim \
  yarn \
  && rm -rf /var/cache/apk/*

WORKDIR /app

ENV EDITOR=vim
ENV RAILS_ENV=development
ENV RACK_ENV=development
ENV RAILS_LOG_TO_STDOUT=true
ENV RAILS_ROOT=/app
ENV LANG=C.UTF-8

# Install gems
COPY Gemfile* /app/
RUN bundle config set --local path 'vendor/bundle' && \
      bundle config build.nokogiri --use-system-libraries && \
      gem update bundler && \
      bundle install --jobs "$(nproc)" --retry 2

# Install yarn packages
COPY package.json yarn.lock /app/
RUN yarn install --frozen-lockfile

COPY . /app

CMD ["bundle", "exec", "puma", "-b", "tcp://0.0.0.0:3000"]
ENTRYPOINT ["/bin/sh", "/app/docker/docker-entrypoint.sh"]

FROM ruby:$RUBY_VERSION-alpine as production

RUN apk --no-cache upgrade && \
  apk add --no-cache \
  libxml2-dev \
  libxslt-dev \
  postgresql-dev \
  nodejs \
  shared-mime-info \
  tzdata \
  && rm -rf /var/cache/apk/*

WORKDIR /app

ENV GOOGLE_AUTH_ID=auth-id
ENV GOOGLE_AUTH_SECRET=auth-secret
ENV NODE_ENV=production
ENV RAILS_ENV=production
ENV RACK_ENV=production
ENV RAILS_LOG_TO_STDOUT=true
ENV RAILS_SERVE_STATIC_FILES=true
ENV RAILS_ROOT=/app
ENV LANG=C.UTF-8
ENV SECRET_KEY_BASE=something
ENV APPS_H5_DELIVERY_METHOD=letter_opener_web
ENV APPS_H5_APP_HOST=localhost:3000

COPY --from=development /app ./

RUN bundle config set --local path 'vendor/bundle' \
    && bundle config set without 'development test' \
    && bundle clean --force \
    # Remove unneeded files (cached *.gem, *.o, *.c)
    && find /app/vendor/bundle/ -name "*.gem" -delete \
    && find /app/vendor/bundle/ -name "*.c" -delete \
    && find /app/vendor/bundle/ -name "*.o" -delete

RUN apk add --no-cache yarn && \
    RAILS_ENV=production NODE_ENV=production bundle exec rails webpacker:compile && \
    apk del yarn

RUN rm -rf node_modules tmp/* log/* app/assets vendor/dictionary*

EXPOSE 3000

CMD ["bundle", "exec", "puma", "-b", "tcp://0.0.0.0:3000"]
ENTRYPOINT ["/bin/sh", "/app/docker/docker-entrypoint.sh"]
