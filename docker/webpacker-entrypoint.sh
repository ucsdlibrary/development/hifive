#!/bin/sh
set -e

# Wipe existing packs
echo "removing existing packs"
rm -rf /app/public/packs

# Start webpack dev server
echo "Starting webpack-dev-server"
/app/bin/webpack-dev-server
