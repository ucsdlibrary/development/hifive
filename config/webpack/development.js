process.env.NODE_ENV = process.env.NODE_ENV || 'development'

const environment = require('./environment')

const { BundleAnalyzerPlugin } = require("webpack-bundle-analyzer")
environment.plugins.prepend(
  "BundleAnalyzerPlugin", new BundleAnalyzerPlugin({
    analyzerHost: '0.0.0.0'
  })
)

module.exports = environment.toWebpackConfig()
