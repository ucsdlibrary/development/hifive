const { environment } = require('@rails/webpacker')

const webpack = require('webpack')

// Enable the default config
environment.splitChunks()

environment.plugins.prepend(
  'Provide', new webpack.ProvidePlugin({
    $: 'jquery',
    jQuery: 'jquery',
    Popper: ['popper.js', 'default']
  })
)

const MomentLocalesPlugin = require('moment-locales-webpack-plugin')
module.exports = {
    plugins: [
        // To strip all locales except “en”
        new MomentLocalesPlugin(),
    ],
}

module.exports = environment
