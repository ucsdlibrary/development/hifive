source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 6.0.3'
gem 'webpacker', '~> 5.x'
# Use postgresql as the database for Active Record
gem 'pg', '>= 0.18', '< 2.0'
# Use Puma as the app server
gem 'puma', '~> 5.0'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'mini_racer', platforms: :ruby

# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.9'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'
gem 'net-ldap', '~> 0.16.2'
gem 'omniauth', '~> 1.9'
gem 'omniauth-google-oauth2', '~> 0.8'
gem 'okcomputer', '~> 1.0'
# Simple Form, Bootstrap and friends
gem 'simple_form', '~> 5.0'

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.1.0', require: false

gem 'pagy', '~> 3.5'
gem 'listen', '>= 3.0.5', '< 3.3'
gem 'letter_opener_web', '~> 1.0'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem "simplecov", '0.19.1', require: false
  gem 'simplecov-cobertura', '1.4.2'
  gem 'byebug', '11.1.3'
  gem 'capybara', '3.33.0'
  gem 'factory_bot_rails', '6.1.0'
  gem 'pry-byebug', '3.9.0'
  gem 'pry-rails', '0.3.9'
  gem 'pry-doc', '1.1.0'
  gem 'rspec-rails', '4.0.1'
  gem 'rubocop', '1.2.0'
  gem 'rubocop-performance', '1.9.0'
  gem 'rubocop-rspec', '2.0.0'
  gem 'selenium-webdriver', '3.142.7'
  gem 'dotenv-rails', '2.7.6'
end

group :development do
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring', '2.1.1'
  gem 'spring-watcher-listen', '~> 2.0.0'
end


# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
