/* eslint no-console:0 */
import 'src/globals'
import 'popper.js'
import 'bootstrap'
import moment from 'moment'
moment.locale('en')

import 'bootstrap-daterangepicker'

jQuery(document).ready(function(){
  $('#start_date').daterangepicker({
    singleDatePicker: true,
    startDate: moment().startOf('month'),
    minYear: 2019, //app first deployed in 2019
    maxYear: new Date().getFullYear(),
    showDropdowns: true,
    locale: {
      format: 'YYYY-MM-DD'
    }
  })
  $('#end_date').daterangepicker({
    singleDatePicker: true,
    minYear: 2019, //app first deployed in 2019
    maxYear: new Date().getFullYear(),
    showDropdowns: true,
    locale: {
      format: 'YYYY-MM-DD'
    }
  })
})
