/* eslint no-console:0 */

// global imports for all packs
import 'core-js/stable'
import 'regenerator-runtime/runtime'
import Rails from '@rails/ujs'

import 'css/application'

Rails.start()

